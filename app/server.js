import bodyParser from 'body-parser';
import bs from 'browser-sync';
import compression from 'compression';
import cookieParser from 'cookie-parser';
import express from 'express';
import morgan from 'morgan';
import stylus from 'stylus';

import * as db from './db.js';

const
  app = express(),
  env = process.argv[2] || `prod`,
  port = (env === `dev`) ? 2368 : 3000,
  viewPath = `${__dirname}/app/src/jade`;

app.set(`views`, viewPath);
app.set(`view engine`, `jade`);

app.use(compression());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(morgan(`combined`));
app.use(stylus.middleware({
  src: `${__dirname}/app/src/stylus`,
  dest: `${__dirname}/app/dist/css`,
  compile: (str, path) => {
    return stylus(str)
      .set(`filename`, path)
      .set(`compress`, false)
  }
}));

app.get(`/`, (req, res) => {
  res.render(`index`, {authorized: false});
});

app.get(`/login`, (req, res) => {
  res.render(`login`, {fail: false});
});

app.post(`/login`, (req, res) => {
  const
    logon_user = req.body.username,
    logon_pass = req.body.password;

  console.log(`${logon_user}`);
  if (logon_user === `admin` && logon_pass === `password`) {
    res.redirect(`/auth/success`);
  } else {
    res.redirect(`/auth/fail`);
  }
});

app.get(`/logout`, (req, res) => {
  res.redirect(`/`);
});

app.get(`/auth/success`, (req, res) => {
  res.render(`index`, {authorized: true});
});

app.get(`/auth/fail`, (req, res) => {
  res.render(`login`, {fail: true});
});

app.use(express.static(`${__dirname}/app/dist`));

app.listen(port, function() {
  if (env === `dev`) {
    bs({
      files: [`app/src/*/*.{jade,styl}`],
      open: false,
      proxy: `localhost:${port}`
    });
    console.log(`listening on port 3000 w/ browser-sync`);
  } else {
    console.log(`listening on port ${port}`);
  }
});
