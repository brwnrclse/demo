const
  records = [
    { id: 1, username: 'admin', password: '****', displayName: 'admin', email: 'jack@demo.com' },
    { id: 2, username: 'user', password: '***', displayName: 'user', email: 'user@demo.com'}
  ],
  findById = function(id, fn) {
    process.nextTick(() => {
      let idx = id - 1;
      if (records[idx]) {
        fn(null, records[idx]);
      } else {
        fn(new Error(`User ${id} does not exist`));
      }
    });
  },
  findByUsername = function(u, fn) {
    process.nextTick(() => {
      for (let i = 0, len = records.length; i < len; i++) {
        let record = records[i];
        if (record.username === u) {
          return fn(null, record);
        }
      }
      return fn(null, null);
    });
  };

export default {
  findById,
  findByUsername
};
